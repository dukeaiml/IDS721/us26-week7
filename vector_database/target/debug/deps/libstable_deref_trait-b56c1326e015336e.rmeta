rust     E�#rustc 1.75.0 (82e1608df 2023-12-21)���j�XX�R��T�MD	 -b0083070c892a1db� ��bl}�wB�?+p{��� -2447397acf63b01e� ����er!�M��I -5816c590a0da89c2�  rustc_std_workspace_core��Fo�M�"�g	BBm^� -3b6c10a2acaa607f� �-���P�zgZ�A�
{ -edb678dd3e28691a� �DC�}νot,�虛�[ -50e20e60add24734� ��?~Vc���G���� -46eaa7bd445cb528�  cfg_if�����N�Vݵ�)E5�� -3c8a48285a1e7255�  miniz_oxide�(Dd��V���\S��'u -6d82e7a8c3f5e2c7� adler�>Yp����c	C�ÈQ� -e66d24d044cc2029�  	hashbrown��Htc�+ М�t�: -4802352fcc77de56� rustc_std_workspace_alloc��L�㴩c��t(� -e71c86d9086176a7�  
std_detect��MX	�B\db��L�(� -dea09910a3b22702� rustc_demangle�Q��U^�Z�=�@ӷ� -e1d006f163566466� 	addr2line���S��b���@�o�� -e19e4ea986b9addc� gimli�g\�L�|��w�w��u  -363744fff3c4e7ba�  object�_�䅼WM
_(�'E	 -f163e9d1987a8318� memchr��S���bD,g-6A� -350512940f04084a� �StS}Y����?�E9[�� -8282820217d7b362�      �  �  �    StableDeref�   CloneStableDeref�         	 
         �  �       �  "�  $�  	&�  
( 'a� (�  +�	 +�  .�	 .�  1�	 1�  4�	 4�  7�	 7�  :�	 :�  =�	 =�  �� � Box� ������ l     ��Ս�д� ��8�8�8 Global�        ����ݶ��&  �6�6 � �6 buf��1�6��1      ۺ������+�� �:�: � �:��+      ����ۈ�a  �9�9 i �9 inner��$      ��ћʾ��  �� � ���      �񝿦���  �&�& � �&��%     �����ٚ��  �%�% � �%�	�%�% phantom��%�%��%      ���������� � �.�. G �.�	�.�.��.�.��.      ץ�Ɂ������ � �"�"  Ref� �" value�� �" borrow��       ���������   ) �	� �"�"  RefMut� �"�� �"�� �"��       ���Ÿ���.   , �	� �E�E � �E lock��0�E poison��0      ��Ū�ٔ��   / �	� �E�E � �E data��2�E 
inner_lock��2      ʾ������b   2 �	� �E�E � �E��2�E��2      �ԃ�����r   5 �	�   8 �	�    ; �	�    > �	�7
 �4�E(�"$�.�61�E�9+�"��:=
�&.�E �%&�.:
 "�% f V�       �  �
       �  �
       z  �  �    �
  �3  �    �  �3  �%    	G  �4  �.    
�  �4  �6    �  �5  �:    i  �5  �9    �  �5  �    �  �5  �&    �  �6
  �E    �  �6  �E    �  �6  �E    �  �6  �"    �  �6  �"      ��>  �
This module defines an unsafe marker trait, StableDeref, for container types that deref to a fixed address which is valid even when the containing type is moved. For example, Box, Vec, Rc, Arc and String implement this trait. Additionally, it defines CloneStableDeref for types like Rc where clones deref to the same address.

It is intended to be used by crates such as [owning_ref](https://crates.io/crates/owning_ref) and [rental](https://crates.io/crates/rental), as well as library authors who wish to make their code interoperable with such crates. For example, if you write a custom Vec type, you can implement StableDeref, and then users will be able to use your custom type together with owning_ref and rental.

no_std support can be enabled by disabling default features (specifically "std"). In this case, the trait will not be implemented for the std types mentioned above, but you can still use it for your own types.
�  ��    � �      � �      � �      z � �    �
 � �    � � �%    	G � �.    
� � �6    � � �:    i � �9    � � �    � � �&    � � �E    � � �E    � � �E    � � �"    � � �"     � �  �
  �  �
  �  �  �  �,#  �!
An unsafe marker trait for types that deref to a stable address, even when moved. For example, this is implemented by Box, Vec, Rc, Arc and String, among others. Even when a Box is moved, the underlying storage remains at a fixed location.

More specifically, implementors must ensure that the result of calling deref() is valid for the lifetime of the object, not just the lifetime of the borrow, and that the deref is valid even if the object is moved. Also, it must be valid even after invoking arbitrary &self methods or doing anything transitively accessible from &Self. If Self also implements DerefMut, the same restrictions apply to deref_mut() and it must remain valid if anything transitively accessible from the result of deref_mut() is mutated/called. Additionally, multiple calls to deref, (and deref_mut if implemented) must return the same address. No requirements are placed on &mut self methods other than deref_mut() and drop(), if applicable.

Basically, it must be valid to convert the result of deref() to a pointer, and later dereference that pointer, as long as the original object is still live, even if it has been moved or &self methods have been called on it. If DerefMut is also implemented, it must be valid to get pointers from deref() and deref_mut() and dereference them while the object is live, as long as you don't simultaneously dereference both of them.

Additionally, Deref and DerefMut implementations must not panic, but users of the trait are not allowed to rely on this fact (so that this restriction can be removed later without breaking backwards compatibility, should the need arise).

Here are some examples to help illustrate the requirements for implementing this trait:

```
# use std::ops::Deref;
struct Foo(u8);
impl Deref for Foo {
    type Target = u8;
    fn deref(&self) -> &Self::Target { &self.0 }
}
```

Foo cannot implement StableDeref because the int will move when Foo is moved, invalidating the result of deref().

```
# use std::ops::Deref;
struct Foo(Box<u8>);
impl Deref for Foo {
    type Target = u8;
    fn deref(&self) -> &Self::Target { &*self.0 }
}
```

Foo can safely implement StableDeref, due to the use of Box.


```
# use std::ops::Deref;
# use std::ops::DerefMut;
# use std::rc::Rc;
#[derive(Clone)]
struct Foo(Rc<u8>);
impl Deref for Foo {
    type Target = u8;
    fn deref(&self) -> &Self::Target { &*self.0 }
}
impl DerefMut for Foo {
    fn deref_mut(&mut self) -> &mut Self::Target { Rc::make_mut(&mut self.0) }
}
```

This is a simple implementation of copy-on-write: Foo's deref_mut will copy the underlying int if it is not uniquely owned, ensuring unique access at the point where deref_mut() returns. However, Foo cannot implement StableDeref because calling deref_mut(), followed by clone().deref() will result in mutable and immutable references to the same location. Note that if the DerefMut implementation were removed, Foo could safely implement StableDeref. Likewise, if the Clone implementation were removed, it would be safe to implement StableDeref, although Foo would not be very useful in that case, (without clones, the rc will always be uniquely owned).


```
# use std::ops::Deref;
struct Foo;
impl Deref for Foo {
    type Target = str;
    fn deref(&self) -> &Self::Target { &"Hello" }
}
```
Foo can safely implement StableDeref. It doesn't own the data being derefed, but the data is gaurenteed to live long enough, due to it being 'static.

```
# use std::ops::Deref;
# use std::cell::Cell;
struct Foo(Cell<bool>);
impl Deref for Foo {
    type Target = str;
    fn deref(&self) -> &Self::Target {
        let b = self.0.get();
        self.0.set(!b);
        if b { &"Hello" } else { &"World" }
    }
}
```
Foo cannot safely implement StableDeref, even though every possible result of deref lives long enough. In order to safely implement StableAddress, multiple calls to deref must return the same result.

```
# use std::ops::Deref;
# use std::ops::DerefMut;
struct Foo(Box<(u8, u8)>);
impl Deref for Foo {
    type Target = u8;
    fn deref(&self) -> &Self::Target { &self.0.deref().0 }
}
impl DerefMut for Foo {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0.deref_mut().1 }
}
```

Foo cannot implement StableDeref because deref and deref_mut return different addresses.


�   ��!     �,                 �    �-             �B �A  �16  �
An unsafe marker trait for types where clones deref to the same address. This has all the requirements of StableDeref, and additionally requires that after calling clone(), both the old and new value deref to the same address. For example, Rc and Arc implement CloneStableDeref, but Box and Vec do not.

Note that a single type should never implement both DerefMut and CloneStableDeref. If it did, this would let you get two mutable references to the same location, by cloning and then calling deref_mut() on both values.
�   �-�     �1                  �B   �1    ��B   �1             �G �F �G �F  �3  �  �3  �  �4  �  �4  �  �4  �  �5  � � � � �  �5  �  �6:  � � � � � � �  �6  � � � � �  �7-    �6 � �HQ    �6   �7    �   �6      �6   �   �6       �6    �  �            �  �7  �I            �7%    �7 � �I\    �7   �7    �   �7      �7   �   �7       �7    �  �              ��   �7 � �J �J            �8"    �7 � �Je    �8   �8    �   �8      �8   �   �8       �7    �         �  �8#    �8 � �Kk    �8   �8    �   �8      �8   �   �8       �8    �         �  �9$    �8 � �Lq    �9   �9    �   �9      �9   �   �9       �8    �         �  �9#    �9 � �Mw    �9   �9    �   �9      �9   �   �9       �9    �         �  �:,    �9 � �M}    �:   �:    �   �:      �:   �   �:       �9    �  � !     !      �  �:  �N            �:1    �: � �N�    �:   �:    �   �:      �:   �   �:       �:    �  � #     #      �  �:  �O            �;-    �; � �P�    �;   �;    �   �;      �;   �   �;       �;    �  � %     %      �  �;  �P            �;2    �; � �Q�    �;   �;    �   �;      �;   �   �;       �;    �  � '     '      �  �<  �R            �<5    �  �	 )   � *    )  *     �  �<  �R  �<  �R            �<8    �  �	 ,   � -    ,  -     �  �<  �S  �<  �S            �=<    �= � �S�    �=   �=    �   �=      �=   �   �=       �=    �  �	 /   � 0    0 /      �  �=  �T  �=  �T            �>A    �= � �T�    �>   �>    �   �>      �>   �   �>       �=    �  �	 2   � 3    2  3     �  �>  �U  �>  �U            �>B    �> � �U�    �>   �>    �   �>      �>   �   �>       �>    �  �	 5   � 6    5  6     �  �>  �V  �?  �W            �?0    �  �	 8   � 9    8  9     �  �?  �W  �?  �W            �?5    �  �	 ;   � <    < ;      �  �?  �X  �?  �X            �@4    �  �	 >   � ?    >  ?     �  �@  �X  �@  �X           #https://crates.io/crates/owning_ref�  rental�  
owning_ref�   https://crates.io/crates/rental� �Y  �Y �Y  �Y �Y  �Y �Y �Y ���0��	)7�����0���.��z�@���0���}ۣf�����0�ȿ��G�F���0��\ˈ#���0���[��ݧ���0�N0��҄m���0�|�م�ч���0�<���ɸ����0����}����0�48�\�s�4���0�����LQNo���0�"��>e�k"���0�a�Q�������0�Uu���9y���0�uW�{4���0���BJl����0�zkLD��-���0��l��Eߋg���0�|��4�Yb���0�bq�JÐݧ���0����2����0�&�H��YV����0�����T�����0��[ßQ>B���0��j�/�����0�җU=�_����0���]�dհ���0����ܑ����0�E���Yߊ���0����g����0���w���.����0��6k"D>����0���AR�����0��~�,_�9����0��ǽـ%�	���0���I��q�}���0���pM_�ͼ���0�a���S�1���0�� �y�j����0������I}���0����R�C�$���0��x��'H/���0�[m��2u����0���{��VI����0��߱�������0�k5�(˕�����0�`W�ɭ����0���;l0��Z���0�!5�򜐇���0�ٲ�S�+����0�#ɗ� ��!���0�`l_w�`�d���0�(}��\�����0���ЮX��J���0�ob-�נG���0�/;�>�?����0��JW�/	����0���X���h���0�FJ�0_(c���0�O�bb8��{���0�310�%������0��C���o���0�^�5 ��9��
               �!                                                $   �$   A%�%&�&�&   y'   (   �(     )      m )      �)      \*      �*      � +      � +      : ,�                !� #                   !� #                                                � $   * %   � % &{ &� &^ '   � '   x (    )   H )      � )      7 *      � *      { +      � +       ,      b ,�          � =#�
������!�#�#�#�#�#�#�#�#�#�#�#�#�#�#$$$�$�$*%:%�%&{&�&^'r'�'�'x(�())H)R)f)�)�)�)7*A*U*�*�*�*{+�+�+�+�+�+,,3,b,l,  ������ 6#�#�#�#�#�#�#�#�#�#�#�#�#�#$$$q$�$�$-%�%&m&�&C'e'�'�']((�()")O)Y)o)�)�)*>*H*�*�*�*U+�+�+�+�+�+�+,&,<,i,s,            � Q#                                �$�$%7%�%&w&�&Z'o'�'�'t(�())D)  c)�)  �)3*  R*�*  �*w+  �+�+  �+,  0,^,  },            � >#                                t$�$ %0%�%&p&�&F'h'�'�'`(�(�()%)  \)r)  �)*  K*�*  �*X+  �+�+  �+�+  ),?,  v,            !�#                                                �$  (%  �%&y&�&\'  �'  v(  )  F)    �)    5*    �*    y+    �+    ,    `,                                                -  �  �>l�  �  �  9  ?    �    �    5    �    �    �    �                                                  �$  9%          q'  �'  �(  )    e)    �)    T*    �*    �+    �+    2,    ,                                                                                                                                                � v#�          � 4#                                o$  �$  �% &k&�&A'  �'  [(  �(   )    m)    *    �*    S+    �+    �+    :,��������#'+/37;?CGKRV]aeimqx|��������������������� $�,                �  �                            �  ��      ���0��j�Kb��>8N8F8V8y8i8�8�ODHT @       �          ��  ���0��ǽـ%�	#   ���0�`l_w�`�d4                                                               ���0�|�م�ч                                                                                                       ���0��C���o>   ���0�<���ɸ�                                                                                   ���0����ܑ�   ���0���BJl�   ���0�a�Q����   ���0����2�                       ���0����R�C�$)                                                               ���0���.��z�@   ���0�`W�ɭ�/                                                               ���0��[ßQ>B   ���0���[��ݧ   ���0�#ɗ� ��!3   ���0��6k"D>�    ���0���}ۣf��                       ���0�310�%���=                       ���0�48�\�s�4
   ���0�ٲ�S�+�2                                                                                                                                               ���0�[m��2u�+   ���0���;l0��Z0   ���0�&�H��YV�   ���0�FJ�0_(c;                                           ���0�a���S�1&   ���0��	)7��    ���0�O�bb8��{<   ���0�|��4�Yb                       ���0��x��'H/*   ���0������I}(   ���0�җU=�_�   ���0�/;�>�?�8                       ���0�����T��   ���0�uW�{4                       ���0�bq�JÐݧ   ���0�zkLD��-   ���0��l��Eߋg   ���0���I��q�}$   ���0�ȿ��G�F   ���0���X���h:                                           ���0��\ˈ#   ���0�����LQNo   ���0����}�	   ���0���w���.�   ���0���{��VI�,   ���0�k5�(˕��.   ���0��߱����-   ���0���AR��!                                           ���0����g�                       ���0�ob-�נG7   ���0���ЮX��J6   ���0�E���Yߊ                                           ���0�(}��\��5                                           ���0��~�,_�9�"   ���0���pM_�ͼ%   ���0�^�5 ��9?                                           ���0���]�dհ   ���0�"��>e�k"                                                                                                       ���0��j�/��                                                                                                       ���0��JW�/	�9   ���0�!5�򜐇1                                                                                                       ���0�N0��҄m   ���0�� �y�j�'                                                                                                       ���0�Uu���9y   2���C�����7����W`c���� |���!S~K�Y�P�������N-N1��M=�>Bi�{ �S3>#4��7\hjL~��S�%E��a��Q^��X�����@�����|C�����6D�����<2���C�����7���  l/Users/udyansachdev/.cargo/registry/src/index.crates.io-6f17d22bba15001f/stable_deref_trait-1.2.0/src/lib.rs�  �m���o]&�3+��                �@�   E B @ A $   F � �   +            �  � � �  X       1    r       2    =           2   O    �       2   �        '   ,    �        ;   O    Y    '   / �  :  N  N              #    @      1  )  &  '  (  '   0  5  1  6  9 <  @  E  F  4 9   !e~�_�g�q�
��?  _C aarch64-apple-darwin�|U�^"Ca���_p�F stable_deref_trait� -b56c1326e015336e����0�       ӊ         �	�     �G            @   �                >�>@�@@@�  �         @ @��  >   |      >   |@   �                     > >>>>  >                  >   |   @   �                          a�       